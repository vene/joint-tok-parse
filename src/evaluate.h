#pragma once

#include <vector>
#include "utils.h"
#include "tag_scheme.h"

typedef std::pair<size_t, size_t> span_t;

template<class T>
void
count_sets(
    std::vector<T> gold,
    std::vector<T> pred,
    size_t& tp,
    size_t& fp,
    size_t& fn)
{
    auto gold_first = gold.begin();
    auto gold_last = gold.end();
    auto pred_first = pred.begin();
    auto pred_last = pred.end();

    while (gold_first != gold_last && pred_first != pred_last)
    {
        if (*gold_first < *pred_first)
        {
            ++gold_first;
            ++fn;  // missed a gold segment
        }
        else if (*pred_first < *gold_first)
        {
            ++pred_first;
            ++fp;  // predicted a spurious segment
        }
        else
        {
            ++gold_first;
            ++pred_first;
            ++tp; // correct prediction, yay
        }
    }

    // count the leftovers
    while (gold_first < gold_last)
    {
        ++gold_first;
        ++fn;
    }

    while (pred_first < pred_last)
    {
        ++pred_first;
        ++fp;
    }
}


struct SegmentF1
{
    int n_tags_;
    std::vector<size_t> tp, fp, fn;

    explicit
    SegmentF1(
        unsigned n_tags)
        : n_tags_(n_tags)
        , tp(1 + n_tags, 0)
        , fp(1 + n_tags, 0)
        , fn(1 + n_tags, 0)
    { }

    void
    update(
        const Sentence& gold_sent,
        const vector<segment_t> pred)
    {
        auto gold = gold_sent.segments();

        unsigned tag;
        size_t a, b;
        // first: regardless of tag, just segmentation
        {
            vector<span_t> gold_, pred_;
            for (auto && s : gold)
            {
                std::tie(tag, a, b) = s;
                gold_.push_back(std::make_pair(a, b));

                // std::cout << "(" << tag
                          // << "," << a
                          // << ":" << b
                          // << "), ";
            }
            // std::cout << std::endl;
            for (auto && s : pred)
            {
                std::tie(tag, a, b) = s;
                pred_.push_back(std::make_pair(a, b));
                // std::cout << "(" << tag
                          // << "," << a
                          // << ":" << b
                          // << "), ";
            }
            // std::cout << std::endl;
            count_sets(gold_, pred_, tp[0], fp[0], fn[0]);
        }

        // now for each class
        for (int k = 0; k < n_tags_; ++k)
        {
            vector<segment_t> gold_, pred_;
            for (auto && s : gold)
                if (std::get<0>(s) == k)
                    gold_.push_back(s);
            for (auto && s : pred)
                if (std::get<0>(s) == k)
                    pred_.push_back(s);
            count_sets(gold_, pred_, tp[1 + k], fp[1 + k], fn[1 + k]);
        }

    // for (int k = 0; k < n_tags_ + 1; ++k)
        // std::cout << tp[k] << " " << fp[k] << " " << fn[k] << std::endl;

    }

    void
    segment_prf(float& p, float& r, float& f)
    {
        p = tp[0] / (tp[0] + fp[0] + 1e-30);
        r = tp[0] / (tp[0] + fn[0] + 1e-30);
        f = 2 * p * r / (p + r + 1e-30);
    }

    void
    micro_prf(float& p, float& r, float& f)
    {
        size_t tp_sum = 0, fp_sum = 0, fn_sum = 0;
        for (int k = 1; k < n_tags_ + 1; ++k)
        {
            tp_sum += tp[k];
            fp_sum += fp[k];
            fn_sum += fn[k];
        }

        p = tp_sum / (tp_sum + fp_sum + 1e-30);
        r = tp_sum / (tp_sum + fn_sum + 1e-30);
        f = 2 * p * r / (p + r + 1e-30);
    }
};
