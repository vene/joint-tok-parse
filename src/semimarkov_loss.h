#pragma once

#include <vector>
#include <string>
#include <Eigen/Dense>
#include <dynet/nodes-impl-macros.h>

#include "semimarkov.h"
#include "sparsemap_loss.h"
#include "tag_scheme.h"

namespace ei = Eigen;


struct SemimarkovSparseMAPLoss : public SparseMAPLoss
{
    std::vector<Segment> y_true_;

    size_t length_, n_states_, n_segments_, max_segment_;

    explicit
    SemimarkovSparseMAPLoss(
        const std::initializer_list<dy::VariableIndex>& a,
        size_t length,
        size_t n_states,
        std::vector<segment_t> y_true,
        size_t n_segments,
        size_t max_segment,
        bool cost_augment)
        : SparseMAPLoss(a, cost_augment)
        , length_(length)
        , n_states_(n_states)
        , n_segments_(n_segments)
        , max_segment_(max_segment)
    {
        n_unary = n_segments * n_states;
        n_add = n_states * (2 + n_states);

        unsigned state;
        size_t start, cease;
        for (auto && seg : y_true)
        {
            std::tie(state, start, cease) = seg;
            if (cease - start + 1 > max_segment_)
                y_true_.push_back(Segment(start, start + max_segment - 1, state));
            else
                y_true_.push_back(Segment(start, cease, state));
        }

        true_cfg = static_cast<AD3::Configuration>(&y_true_);
    }

    virtual
    std::string
    get_name()
    const override
    {
        return "segment";
    }

    virtual
    void
    run_dimension_check(
        const std::vector<Dim> &xs)
    const override
    {
        DYNET_ARG_CHECK(xs.size() == 2, "segment loss requires two inputs");
        DYNET_ARG_CHECK(xs[0][0] == n_states_, "unaries have wrong first dim");
        DYNET_ARG_CHECK(xs[0][1] == n_segments_, "unaries have wrong second dim");
        DYNET_ARG_CHECK(xs[1][0] == n_add, "additionals have wrong dimension");
    }

    virtual
    std::unique_ptr<AD3::GenericFactor>
    build_factor()
    const
    {
        using AD3::GenericFactor;

        std::unique_ptr<GenericFactor> segm(new FactorSegmentationTrans);
        (static_cast<FactorSegmentationTrans*>(segm.get()))->Initialize(
            length_,
            n_states_,
            max_segment_ > 0 ? max_segment_ : length_);
        return segm;
    }

};


dy::Expression
semimarkov_sparsemap_loss(
    const dy::Expression& x_unary,
    const dy::Expression& x_add,
    std::vector<segment_t> y_true,
    size_t length,
    size_t max_segment)
{
    auto cg = x_unary.pg;
    auto d = x_unary.dim();

    return dy::Expression(
        cg,
        cg->add_function<SemimarkovSparseMAPLoss>(
            {x_unary.i, x_add.i},
            length,
            d[0],  // n_states
            y_true,
            d[1],  // n_segments
            max_segment,
            false));
}


vector<segment_t>
semimarkov_decode(
    ei::MatrixXf x_unary,
    ei::VectorXf x_add,
    size_t length,
    size_t max_segment)
{
    size_t n_tags = x_unary.rows();
    size_t n_segments = x_unary.cols();

    vector<double> x_u(x_unary.data(), x_unary.data() + n_segments * n_tags);
    vector<double> x_v(x_add.data(), x_add.data() + (n_tags + 2) * n_tags);

    auto segm = FactorSegmentationTrans();
    segm.Initialize(
        length,
        n_tags,
        max_segment > 0 ? max_segment : length);

    vector<Segment> y_pred;
    AD3::Configuration cfg = static_cast<AD3::Configuration>(&y_pred);
    double val;
    segm.Maximize(x_u, x_v, cfg, &val);

    vector<segment_t> out;
    for (auto && seg : y_pred)
        out.push_back(std::make_tuple(seg.state, seg.start, seg.cease));

    return out;
}
