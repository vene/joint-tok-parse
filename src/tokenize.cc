#include <dynet/dynet.h>
#include <dynet/globals.h>
#include <dynet/training.h>
#include <dynet/timing.h>

#include <fstream>

#include "utils.h"
#include "args.h"
#include "tokenize.h"
#include "evaluate.h"

namespace dy = dynet;

using std::cout;
using std::endl;


int main(int argc, char** argv)
{
    auto dyparams = dy::extract_dynet_params(argc, argv);
    CommonArgs args;
    args.parse(argc, argv);
    cout << args << endl;

    if (args.override_dy)
    {
        dyparams.random_seed = 42;
        dyparams.autobatch = true;
    }

    dy::initialize(dyparams);

    std::stringstream vocab_fn, posvocab_fn, train_fn, valid_fn;
    vocab_fn << "../data/" << args.lang << "-vocab.txt";
    posvocab_fn << "../data/" << args.lang << "-posvocab.txt";
    train_fn << "../data/" << args.lang << "-train.txt";
    valid_fn << "../data/" << args.lang << "-valid.txt";

    auto vocab = read_vocab(vocab_fn.str());
    unsigned vocab_size = vocab.size();

    auto posvocab = read_vocab(posvocab_fn.str());
    unsigned posvocab_size = posvocab.size() - 3;

    auto train_batches = read_batches(train_fn.str(), args.batch_size);
    auto valid_batches = read_batches(valid_fn.str(), args.batch_size);
    auto n_batches = train_batches.size();

    unsigned n_train_sents = 0;
    for (auto&& batch : train_batches)
        n_train_sents += batch.size();

    unsigned n_valid_words = 0;
    for (auto&& batch : valid_batches)
        for (auto&& sent : batch)
            n_valid_words += sent.tok_lengths.size();

    vector<vector<vector<Sentence> >::iterator> train_iter(n_batches);
    std::iota(train_iter.begin(), train_iter.end(), train_batches.begin());

    dy::ParameterCollection params;
    std::unique_ptr<BaseTokenizer> tok;

    if (args.method == "seq")
        tok.reset(new SequenceTokenizer(
            params,
            posvocab_size,
            vocab_size,
            args.hidden_dim,
            args.scheme));

    else if (args.method == "simple")
        tok.reset(new SimpleTokenizer(
            params,
            posvocab_size,
            vocab_size,
            args.hidden_dim,
            args.scheme));

    else if (args.method == "segment")
        tok.reset(new SegmentTokenizer(
            params,
            posvocab_size,
            vocab_size,
            args.hidden_dim));

    dy::SimpleSGDTrainer trainer(params, args.lr);
    float best_micro_f = 0;
    unsigned impatience = 0;

    for (unsigned it = 0; it < args.max_iter; ++it)
    {
        // shuffle the permutation vector
        std::shuffle(train_iter.begin(), train_iter.end(), *dy::rndeng);

        float total_loss = 0;

        {
            std::unique_ptr<dy::Timer> timer(new dy::Timer("train took"));
            for (auto&& batch : train_iter)
            {
                dy::ComputationGraph cg;
                auto loss = tok->batch_loss(cg, *batch);
                auto loss_val = dy::as_scalar(cg.incremental_forward(loss));
                total_loss += loss_val;
                cg.backward(loss);
                trainer.update();
            }
        }

        SegmentF1 accum(posvocab_size);
        {
            std::unique_ptr<dy::Timer> timer(new dy::Timer("valid took"));
            for (auto&& batch : valid_batches)
            {
                dy::ComputationGraph cg;
                tok->count_correct(cg,
                                   batch,
                                   accum);
            }
        }

        float f, p, r, fm, pm, rm;
        accum.segment_prf(p, r, f);
        accum.micro_prf(pm, rm, fm);

        cout << "epoch " << it << endl
             << " train loss " << total_loss / n_train_sents << endl
             << "    segm P: " << std::setprecision(4) << p
             << "\tR: "          << std::setprecision(4) << r
             << "\tF: "          << std::setprecision(4) << f
             << endl
             << "   micro P: " << std::setprecision(4) << pm
             << "\tR: " << std::setprecision(4) << rm
             << "\tF: " << std::setprecision(4) << fm
             << endl;

        if (fm >= best_micro_f)
        {
            impatience = 0;
            best_micro_f = fm;
        }
        else
        {
            trainer.learning_rate *= args.decay;
            cout << "Decaying LR to " << trainer.learning_rate << endl;
            impatience += 1;
        }

        if (impatience > args.patience)
        {
            cout << args.patience << " epochs without improvement." << endl;
            return 0;
        }

        /*
        std::stringstream out_fn;
        out_fn << "tokenize_" << args.lang
               << "_f1_"
               << std::internal
               << std::setfill('0')
               << std::fixed << std::setprecision(2)
               << std::setw(5)
               << valid_f
               << "_epoch_"
               << std::setw(3) << it
               << ".dy";
        segmenter->save(out_fn.str());
        */

    }
    return 0;
}
