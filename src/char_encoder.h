#pragma once

/* Common files for encoding a list of characters into vectors */

namespace dy = dynet;

enum Special {SPACE = 0, UNK, BOS, EOS};

struct BiLSTMCharEncoder
{
    unsigned vocab_size_;
    unsigned hidden_dim_;

    dy::ParameterCollection p;
    dy::LookupParameter p_emb;
    unique_ptr<dy::RNNBuilder> fw_builder;
    unique_ptr<dy::RNNBuilder> bw_builder;

    explicit BiLSTMCharEncoder(dy::ParameterCollection& params,
                               unsigned vocab_size,
                               unsigned hidden_dim,
                               unsigned out_dim)
        : vocab_size_(vocab_size)
        , hidden_dim_(hidden_dim)
    {
        p = params.add_subcollection("char-encoder");

        const int layers = 1;
        fw_builder.reset(
            new dy::VanillaLSTMBuilder(layers, hidden_dim, out_dim / 2, p));
        bw_builder.reset(
            new dy::VanillaLSTMBuilder(layers, hidden_dim, out_dim / 2, p));

        p_emb = p.add_lookup_parameters(vocab_size_, {hidden_dim_});
    }

    vector<Expression> encode_sentence(dy::ComputationGraph& cg,
                                       const Sentence& sent)
    {
        // get embedding for each char
        vector<Expression> emb;
        emb.push_back(dy::lookup(cg, p_emb, Special::BOS));
        for (auto&& c : sent.char_ixs)
            emb.push_back(dy::lookup(cg, p_emb, c));
        emb.push_back(dy::lookup(cg, p_emb, Special::EOS));

        size_t n_chars = sent.char_ixs.size();
        size_t n_chars_padded = emb.size();

        // pass through LSTM
        vector<Expression> enc_fw(n_chars_padded),
                           enc_bw(n_chars_padded),
                           enc(n_chars);

        fw_builder->start_new_sequence();
        bw_builder->start_new_sequence();
        for (size_t i = 0; i < n_chars_padded; ++i)
        {
            size_t j = n_chars_padded - i - 1;
            enc_fw[i] = fw_builder->add_input(emb[i]);
            enc_bw[j] = fw_builder->add_input(emb[j]);
        }

        for (size_t i = 0; i < n_chars; ++i)
            enc[i] = dy::concatenate({enc_fw[i + 1], enc_bw[i + 1]});

        return enc;
    }

    vector<vector<Expression> >
    encode_batch(
        dy::ComputationGraph& cg,
        const vector<Sentence>& batch)
    {
        fw_builder->new_graph(cg);
        bw_builder->new_graph(cg);

        vector<vector<Expression> > out;
        for (auto && sent : batch)
            out.push_back(encode_sentence(cg, sent));

        return out;
    }

};
