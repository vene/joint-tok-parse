#pragma once

#include <tuple>
#include <algorithm>
#include <unordered_map>

#include <ad3/GenericFactor.h>

using AD3::GenericFactor;
using AD3::Configuration;


struct Segment {
    int start;
    int cease;
    int state;

    Segment() : start(), cease(), state() {}
    Segment(int s, int c, int t) : start(s), cease(c), state(t) {}
};

bool operator==(Segment const& lhs, Segment const& rhs);

bool operator!=(Segment const& lhs, Segment const& rhs);


namespace std
{
    template<> struct hash<Segment> {
            size_t operator() (const Segment & s) const {
                return hash<int>()(s.start) ^
                       hash<int>()(s.cease) ^
                       hash<int>()(s.state);
            }
        };
}

class FactorSegmentation : public GenericFactor {

    protected:

    vector<Segment>* to_segments(Configuration cfg) {
        return static_cast<vector<Segment>*>(cfg);
    }

    virtual double GetSegmentScore(const Segment s,
                           const vector<double> &variable_logp,
                           const vector<double> &additional_logp) {
        return variable_logp[segment_ix_.at(s)];
    }

    virtual double GetInitialScore(
        const Segment s,
        const vector<double> &variable_logp,
        const vector<double> &additional_logp)
    {
        return 0; // to be extended in derived classes
    }

    virtual double GetTransitionScore(
        const Segment s,
        const int prev_state,
        const vector<double> &variable_logp,
        const vector<double> &additional_logp)
    {
        return 0; // to be extended in derived classes
    }

    virtual double GetFinalScore(
        const Segment s,
        const vector<double> &variable_logp,
        const vector<double> &additional_logp)
    {
        return 0; // to be extended in derived classes
    }

    virtual void AddSegmentPosterior(
        const Segment s,
        double weight,
        vector<double> *variable_post,
        vector<double> *additional_post)
    {
        (*variable_post)[segment_ix_.at(s)] += weight;
    }

    virtual void AddInitialPosterior(
        const Segment s,
        double weight,
        vector<double> *variable_post,
        vector<double> *additional_post)
    {
        // noop to be extended in derived classes
        return;
    }

    virtual void AddTransitionPosterior(
        const Segment s,
        const int prev_state,
        double weight,
        vector<double> *variable_post,
        vector<double> *additional_post)
    {
        // noop to be extended in derived classes
        return;
    }

    virtual void AddFinalPosterior(
        const Segment s,
        double weight,
        vector<double> *variable_post,
        vector<double> *additional_post)
    {
        // noop to be extended in derived classes
        return;
    }
    public:

    FactorSegmentation () {}

    virtual ~FactorSegmentation() { ClearActiveSet(); }

    void Initialize(int length, int n_states)
    {
        Initialize(length, n_states, length);
    }

    void Initialize(int length, int n_states, int max_segment)
    {
        length_ = length;
        n_states_ = n_states;
        max_segment_ = max_segment;

        int k = 0;
        for (int start = 0; start < length; ++start)
            for (int cease = start; cease < length; ++cease) {

                if ((cease - start + 1) > max_segment)
                    continue;

                for (int state = 0; state < n_states; ++state) {
                    // cout << "(" << start << ", " << cease << ")["
                          // << state << "] = " << k << endl;
                    segment_ix_[Segment(start, cease, state)] = k++;
                }
            }

        n_vars_ = k;
    }

    void Maximize(const vector<double> &variable_log_potentials,
                  const vector<double> &additional_log_potentials,
                  Configuration &configuration,
                  double *value) {

        vector<vector<double> > values;
        vector<vector<int> > backp_seg_start;
        vector<vector<int> > backp_seg_state;

        values.resize(n_states_);
        backp_seg_start.resize(n_states_);
        backp_seg_state.resize(n_states_);

        for (int state = 0; state < n_states_; ++state) {

            // allocate space
            values[state].resize(length_ + 1);
            backp_seg_start[state].resize(length_ + 1);
            backp_seg_state[state].resize(length_ + 1);

            // set initial conditions
            values[state][0] = 0;
            backp_seg_start[state][0] = -1;
            backp_seg_state[state][0] = 0;
        }

        double total_val, segment_val;
        int start_from;
        Segment segment;
        const double INF = std::numeric_limits<double>::infinity();

        // cross-product for: indented as single for
        for (int cease = 0; cease < length_; ++cease)
        for (int y_curr = 0; y_curr < n_states_; ++y_curr)
        {

            start_from = max(0, cease - max_segment_ + 1);
            values[y_curr][cease + 1] = -INF;

            for (int start = start_from; start <= cease; ++start)
            {
                segment = Segment(start, cease, y_curr);
                segment_val = GetSegmentScore(
                    segment,
                    variable_log_potentials,
                    additional_log_potentials);

                if (start == 0)
                {
                    int y_prev = 0;
                    total_val = values[y_prev][start];
                    total_val += segment_val;
                    total_val += GetInitialScore(segment,
                                                 variable_log_potentials,
                                                 additional_log_potentials);

                    if (cease == length_ - 1)
                        total_val += GetFinalScore(segment,
                                                   variable_log_potentials,
                                                   additional_log_potentials);

                    if (total_val > values[y_curr][cease + 1])
                    {
                        values[y_curr][cease + 1] = total_val;
                        backp_seg_start[y_curr][cease + 1] = start;
                        backp_seg_state[y_curr][cease + 1] = y_prev;
                    }
                }

                else
                {

                    for (int y_prev = 0; y_prev < n_states_; ++y_prev)
                    {
                        total_val = values[y_prev][start];
                        total_val += segment_val;
                        total_val += GetTransitionScore(
                            segment,
                            y_prev,
                            variable_log_potentials,
                            additional_log_potentials);
                        if (cease == length_ - 1)
                            total_val += GetFinalScore(segment,
                                                       variable_log_potentials,
                                                       additional_log_potentials);
                        if (total_val > values[y_curr][cease + 1])
                        {
                            values[y_curr][cease + 1] = total_val;
                            backp_seg_start[y_curr][cease + 1] = start;
                            backp_seg_state[y_curr][cease + 1] = y_prev;
                        }
                    }
                }
            }
        }

        // find best final condition
        int y_best = 0;
        *value = -INF;

        for (int y = 0; y < n_states_; ++y)
        {
            total_val = values[y][length_];
            if (total_val > *value)
            {
                *value = total_val;
                y_best = y;
            }
        }

        // trace back the steps
        int start;
        int cease = length_;
        vector<Segment>* segments = to_segments(configuration);

        while (cease > 0)
        {
            start = backp_seg_start[y_best][cease];
            segments->insert(segments->begin(),
                             Segment(start, cease - 1, y_best));
            y_best = backp_seg_state[y_best][cease];
            cease = start;
        }

        /*
        for (const Segment  &s : *segments)
            cout << "(" << s.start << ", " << s.cease << "): "
                 << s.state << endl;
        */
    }

    void Evaluate(const vector<double> &variable_log_potentials,
                  const vector<double> &additional_log_potentials,
                  const Configuration configuration,
                  double *value) {

        *value = 0;
        const vector<Segment>* segs = to_segments(configuration);

        Segment s = (*segs)[0];
        int prev_state = s.state;

        *value += GetSegmentScore(
            s,
            variable_log_potentials,
            additional_log_potentials);
        *value += GetInitialScore(
            s,
            variable_log_potentials,
            additional_log_potentials);

        for (int k = 1; k < segs->size(); ++k)
        {
            s = (*segs)[k];
            *value += GetSegmentScore(
                s,
                variable_log_potentials,
                additional_log_potentials);

            *value += GetTransitionScore(
                s,
                prev_state,
                variable_log_potentials,
                additional_log_potentials);

            prev_state = s.state;
        }

        *value += GetFinalScore(
            s,
            variable_log_potentials,
            additional_log_potentials);
    }

    void UpdateMarginalsFromConfiguration(
        const Configuration &configuration,
        double weight,
        vector<double> *variable_posteriors,
        vector<double> *additional_posteriors)
    {

        const vector<Segment>* segs = to_segments(configuration);
        Segment s = (*segs)[0];
        int prev_state = s.state;

        AddSegmentPosterior(
            s,
            weight,
            variable_posteriors,
            additional_posteriors);

        AddInitialPosterior(
            s,
            weight,
            variable_posteriors,
            additional_posteriors);

        for (int k = 1; k < segs->size(); ++k)
        {
            s = (*segs)[k];

            AddSegmentPosterior(
                s,
                weight,
                variable_posteriors,
                additional_posteriors);

            AddTransitionPosterior(
                s,
                prev_state,
                weight,
                variable_posteriors,
                additional_posteriors);

            prev_state = s.state;
        }

        // final potential
        AddFinalPosterior(
            s,
            weight,
            variable_posteriors,
            additional_posteriors);
    }

    int CountCommonValues(const Configuration &configuration1,
                          const Configuration &configuration2)
    {
        vector<double> unary;
        vector<double> additionals;
        unary.assign(n_vars_, 0);
        additionals.assign(n_additionals_, 0);
        UpdateMarginalsFromConfiguration(configuration1, 1, &unary, &additionals);
        UpdateMarginalsFromConfiguration(configuration2, 1, &unary, &additionals);
        return count_if(unary.begin(), unary.end(),
                        [](double val){ return val > 1.0; });
    }

    bool SameConfiguration(
        const Configuration &configuration1,
        const Configuration &configuration2)
    {

        const vector<Segment>* seg1 = to_segments(configuration1);
        const vector<Segment>* seg2 = to_segments(configuration2);

        if (seg1->size() != seg2->size())
            return false;

        for (unsigned int k = 0; k < seg1->size(); ++k)
            if ((*seg1)[k] != (*seg2)[k])
                return false;

        return true;
    }

    Configuration CreateConfiguration()
    {
        vector<Segment>* config = new vector<Segment>;
        return static_cast<Configuration>(config);
    }

    void DeleteConfiguration(Configuration configuration)
    {
        vector<Segment>* config = to_segments(configuration);
        delete config;
    }

    protected:
    int n_additionals_ = 0;  // this class doesn't use transitions
    int length_;
    int n_states_;
    int max_segment_; // maximum length of the segments considered
    int n_vars_;      // number of binary variables for given params
    unordered_map<Segment, int> segment_ix_;

};


class FactorSegmentationFull : public FactorSegmentation
{
    public:
    FactorSegmentationFull () {}
    virtual ~FactorSegmentationFull() { ClearActiveSet(); }

    void Initialize(int length, int n_states)
    {
        Initialize(length, n_states, length);
    }

    void Initialize(int length, int n_states, int max_segment)
    {
        FactorSegmentation::Initialize(length, n_states, max_segment);

        transition_ix_.resize(n_states);
        int k = 0;

        // initial
        {
            int start = 0;
            for (int cease = start; cease < length; ++cease)
            {

                if ((cease - start + 1) > max_segment) continue;

                for (int state = 0; state < n_states; ++state)
                {
                    int prev_state = 0;
                    auto s = Segment(start, cease, state);
                    transition_ix_[prev_state][s] = k++;
                }
            }
        }

        // general
        for (int start = 1; start < length; ++start)
        {
            for (int cease = start; cease < length; ++cease)
            {

                if ((cease - start + 1) > max_segment) continue;

                for (int prev_state = 0; prev_state < n_states; ++prev_state)
                for (int state = 0; state < n_states; ++state)
                {
                    auto s = Segment(start, cease, state);
                    transition_ix_[prev_state][s] = k++;
                }
            }
        }

        // final
        int cease = length - 1;
        int start_from = max(0, cease - max_segment_ + 1);
        for (int start = start_from; start < length; ++start)
        {
            for (int state = 0; state < n_states; ++state)
            {
                auto s = Segment(start, cease, state);
                // transition_ix_[prev_state][s] = k++;
                final_ix_[s] = k++;
            }
        }

        n_additionals_ = k;

        // std::cout << n_vars_ << std::endl;
        // std::cout << n_additionals_ << std::endl;

    }



    protected:
    vector<unordered_map<Segment, int> > transition_ix_;
    unordered_map<Segment, int> final_ix_;

    virtual
    double
    GetInitialScore(
        const Segment s,
        const vector<double> &variable_logp,
        const vector<double> &additional_logp)
    override
    {
        return additional_logp[transition_ix_[0].at(s)];
    }

    virtual
    double
    GetTransitionScore(
        const Segment s,
        const int prev_state,
        const vector<double> &variable_logp,
        const vector<double> &additional_logp)
    override
    {
        return additional_logp[transition_ix_[prev_state].at(s)];
    }

    virtual
    double
    GetFinalScore(
        const Segment s,
        const vector<double> &variable_logp,
        const vector<double> &additional_logp)
    override
    {
        return additional_logp[final_ix_.at(s)];
    }

    virtual void AddInitialPosterior(
        const Segment s,
        double weight,
        vector<double> *variable_logp,
        vector<double> *additional_logp)
    override
    {
        (*additional_logp)[transition_ix_[0].at(s)] += weight;
    }

    virtual void AddTransitionPosterior(
        const Segment s,
        const int prev_state,
        double weight,
        vector<double> *variable_logp,
        vector<double> *additional_logp)
    override
    {
        (*additional_logp)[transition_ix_[prev_state].at(s)] += weight;
    }

    virtual void AddFinalPosterior(
        const Segment s,
        double weight,
        vector<double> *variable_logp,
        vector<double> *additional_logp)
    override
    {
        (*additional_logp)[final_ix_.at(s)] += weight;
    }

};


class FactorSegmentationTrans : public FactorSegmentation
{
    public:
    FactorSegmentationTrans () {}
    virtual ~FactorSegmentationTrans() { ClearActiveSet(); }

    void Initialize(int length, int n_states)
    {
        Initialize(length, n_states, length);
    }

    void Initialize(int length, int n_states, int max_segment)
    {
        FactorSegmentation::Initialize(length, n_states, max_segment);

        int k = 0;

        // initial
        init_ix_.resize(n_states, -1);
        for (int state = 0; state < n_states; ++state)
            init_ix_[state] = k++;

        // general
        trans_ix_.resize(n_states);
        for (int prev_state = 0; prev_state < n_states; ++prev_state)
        {
            trans_ix_[prev_state].resize(n_states, -1);

            for (int state = 0; state < n_states; ++state)
                trans_ix_[prev_state][state] = k++;
        }

        // final
        final_ix_.resize(n_states, -1);
        for (int state = 0; state < n_states; ++state)
            final_ix_[state] = k++;

        n_additionals_ = k;

        // std::cout << n_vars_ << std::endl;
        // std::cout << "Factor wants " << n_additionals_ << std::endl;

    }



    protected:
    vector<size_t> init_ix_, final_ix_;
    vector<vector<size_t> > trans_ix_;

    virtual
    double
    GetInitialScore(
        const Segment s,
        const vector<double> &variable_logp,
        const vector<double> &additional_logp)
    override
    {
        return additional_logp[init_ix_[s.state]];
    }

    virtual
    double
    GetTransitionScore(
        const Segment s,
        const int prev_state,
        const vector<double> &variable_logp,
        const vector<double> &additional_logp)
    override
    {
        return additional_logp[trans_ix_[prev_state][s.state]];
    }

    virtual
    double
    GetFinalScore(
        const Segment s,
        const vector<double> &variable_logp,
        const vector<double> &additional_logp)
    override
    {
        return additional_logp[final_ix_[s.state]];
    }

    virtual void AddInitialPosterior(
        const Segment s,
        double weight,
        vector<double> *variable_logp,
        vector<double> *additional_logp)
    override
    {
        (*additional_logp)[init_ix_[s.state]] += weight;
    }

    virtual void AddTransitionPosterior(
        const Segment s,
        const int prev_state,
        double weight,
        vector<double> *variable_logp,
        vector<double> *additional_logp)
    override
    {
        (*additional_logp)[trans_ix_[prev_state][s.state]] += weight;
    }

    virtual void AddFinalPosterior(
        const Segment s,
        double weight,
        vector<double> *variable_logp,
        vector<double> *additional_logp)
    override
    {
        (*additional_logp)[final_ix_[s.state]] += weight;
    }

};
