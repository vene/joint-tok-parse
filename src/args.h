#pragma once
#include <iostream>


struct CommonArgs
{
    bool test = false;
    bool override_dy = true;
    unsigned max_iter = 20;
    unsigned batch_size = 16;
    unsigned hidden_dim = 300;
    unsigned patience = 5;
    float lr = 1.0;
    float decay = 0.9;
    std::string method = "simple";
    std::string scheme = "bio";
    std::string lang;
    std::string saved_model;

    void parse(int argc, char** argv)
    {
        int i = 1;
        while (i < argc)
        {
            std::string arg = argv[i];
            if (arg == "--test")
            {
                test = true;
                i += 1;
            }
            else if (arg == "--no-override")
            {
                override_dy = false;
                i += 1;
            }
            else if (arg == "--lang")
            {
                assert(i + 1 < argc);
                lang = argv[i + 1];
                i += 2;
            }
            else if (arg == "--method")
            {
                assert(i + 1 < argc);
                method = argv[i + 1];
                i += 2;
            }
            else if (arg == "--scheme")
            {
                assert(i + 1 < argc);
                scheme = argv[i + 1];
                i += 2;
            }
            else if (arg == "--saved-model")
            {
                assert(i + 1 < argc);
                saved_model = argv[i + 1];
                i += 2;
            }
            else if (arg == "--max-iter")
            {
                assert(i + 1 < argc);
                std::string val = argv[i + 1];
                std::istringstream vals(val);
                vals >> max_iter;
                i += 2;
            }
            else if (arg == "--hidden-dim")
            {
                assert(i + 1 < argc);
                std::string val = argv[i + 1];
                std::istringstream vals(val);
                vals >> hidden_dim;
                i += 2;
            }
            else if (arg == "--batch-size")
            {
                assert(i + 1 < argc);
                std::string val = argv[i + 1];
                std::istringstream vals(val);
                vals >> batch_size;
                i += 2;
            }
            else if (arg == "--lr")
            {
                assert(i + 1 < argc);
                std::string val = argv[i + 1];
                std::istringstream vals(val);
                vals >> lr;
                i += 2;
            }
            else if (arg == "--decay")
            {
                assert(i + 1 < argc);
                std::string val = argv[i + 1];
                std::istringstream vals(val);
                vals >> decay;
                i += 2;
            }
            else if (arg == "--patience")
            {
                assert(i + 1 < argc);
                std::string val = argv[i + 1];
                std::istringstream vals(val);
                vals >> patience;
                i += 2;
            }
            else
            {
                i += 1;
            }
        }
    }
};


std::ostream& operator << (std::ostream &o, const CommonArgs &args)
{
    o <<  (args.test ? "Test" : "Train") << " mode. "
      << "Arguments:" << std::endl
      << "    Language: " << args.lang << std::endl
      << "      Method: " << args.method << std::endl
      << "      Scheme: " << args.scheme << std::endl
      << "  Batch size: " << args.batch_size << std::endl
      << "  Hidden dim: " << args.hidden_dim << std::endl
      << "  Model file: " << args.saved_model << std::endl
      << "   Max. iter: " << args.max_iter << std::endl
      << "          lr: " << args.lr << std::endl
      << "       decay: " << args.decay << std::endl
      << "    patience: " << args.patience << std::endl;
    return o;
}
