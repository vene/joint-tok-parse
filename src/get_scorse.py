import sys
import os

if __name__ == '__main__':

    lang = sys.argv[1]

    res = []

    for fn in os.listdir():
        if not fn.startswith(lang):
            continue

        best_data = None
        best_f = 0

        epoch_line = None
        segm_line = None
        micro_line = None

        with open(fn) as f:
            for line in f:
                line = line.strip()

                if line.startswith("epoch"):
                    epoch_line = line

                if line.startswith("segm P"):
                    segm_line = line

                if line.startswith("micro P"):
                    micro_line = line
                    f = float(line.split()[-1])
                    if f > best_f:
                        best_f = f
                        best_data = (epoch_line, segm_line, micro_line)

        res.append((best_f, fn) + best_data)

    res.sort()

    for best_f, fn, epoch, segm, micro in res:
        fn = f"{fn:>40s}"
        best_f = f"{best_f:.4f}"
        segm = f"{segm:<40s}"
        micro = f"{micro:<40s}"
        print(fn, best_f, segm, micro, epoch, sep="  |  ")  # micro, epoch)

