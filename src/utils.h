#pragma once

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <limits>
#include <cassert>

#include "tag_scheme.h"

using std::vector;

const auto maxln = std::numeric_limits<std::streamsize>::max();


struct Sentence
{
    std::vector<unsigned> char_ixs;
    std::vector<unsigned> tok_lengths;
    std::vector<unsigned> pos_tags;
    std::vector<int> heads;

    friend std::istream& operator>>(std::istream& in,
                                    Sentence& data);
    friend std::ostream& operator<<(std::ostream& out,
                                    const Sentence& data);

    /* convert token lengths to (first, last) indices
     * note: last is included in the token!
     * */

    /*
    vector<std::pair<size_t, size_t> > tok_ixs() const
    {
        vector<std::pair<size_t, size_t> > indices;
        size_t first = 0;
        for (auto && d : tok_lengths) {
            indices.push_back(std::make_pair(first, first + d - 1));
            first += d;
        }
        return indices;
    }
    */

    size_t size()
    const
    {
        return char_ixs.size();
    }

    vector<segment_t> segments(bool include_special=false) const
    {
        vector<segment_t> seg;
        size_t first = 0;
        for (int i = 0; i < tok_lengths.size(); ++i) {
            auto d = tok_lengths[i];
            auto p = pos_tags[i];

            if (include_special == false)
                p -= 3;

            seg.push_back(std::make_tuple(p, first, first + d - 1));
            first += d;
        }
        return seg;

    }

    vector<int> tok_borders() const
    {
        vector<int> out;
        int tokstart = 0;
        for (int k = 0; k < tok_lengths.size() - 1; ++k)
        {
            tokstart += tok_lengths[k];
            out.push_back(tokstart);
        }
        return out;
    }

    vector<bool> bool_boundaries() const
    {
        vector<bool> out(char_ixs.size() - 1, 0);
        int tokstart = 0;
        for (int k = 0; k < tok_lengths.size() - 1; ++k)
        {
            tokstart += tok_lengths[k];
            out[tokstart - 1] = 1;
        }
        return out;
    }
};


std::istream& operator >>(std::istream& in, Sentence& data)
{
    std::string ixs_buf, lens_buf, pos_buf, heads_buf;
    std::getline(in, ixs_buf, '\t');
    std::getline(in, lens_buf, '\t');
    std::getline(in, pos_buf, '\t');
    std::getline(in, heads_buf, '\t');

    if (!in)  // failed
        return in;

    in.ignore(maxln, '\n');

    {
        std::stringstream ixs(ixs_buf);
        unsigned tmp;
        while(ixs >> tmp)
            data.char_ixs.push_back(tmp);
    }
    {
        std::stringstream lens(lens_buf);
        unsigned tmp;
        while(lens >> tmp)
            data.tok_lengths.push_back(tmp);
    }
    {
        std::stringstream pos(pos_buf);
        unsigned tmp;
        while(pos >> tmp)
            data.pos_tags.push_back(tmp);
    }
    {
        std::stringstream heads(heads_buf);
        int tmp;
        while(heads >> tmp)
            data.heads.push_back(tmp);
    }
    return in;

}


std::ostream& operator<<(std::ostream& out,
                         const Sentence& data)
{
    out << "characters: ";
    for (auto&& i: data.char_ixs)
        out << i << " ";
    out << "\nlengths: ";
    for (auto&& i: data.tok_lengths)
        out << i << " ";
    out << "\nPOS tags: ";
    for (auto&& i: data.pos_tags)
        out << i << " ";
    out << "\nparse tree: ";
    for (auto&& i: data.heads)
        out << i << " ";
    out << std::endl;

    return out;
}


vector<vector<Sentence> > read_batches(const std::string& filename,
                                       size_t batch_size)
{

    vector<vector<Sentence> > batches;

    std::ifstream in(filename);
    assert(in);

    vector<Sentence> curr_batch;

    while(in)
    {
        Sentence s;
        in >> s;
        if (!in) break;

        if (curr_batch.size() == batch_size)
        {
            batches.push_back(curr_batch);
            curr_batch.clear();
        }
        curr_batch.push_back(s);
    }

    // leftover batch
    if (curr_batch.size() > 0)
        batches.push_back(curr_batch);

    return batches;
}


std::vector<std::string> read_vocab(const std::string& filename)
{
    std::ifstream in(filename);
    assert(in);

    std::vector<std::string> vocab;
    std::string tmp;
    while(in)
    {
        std::getline(in, tmp);
        if (!in) break;
        vocab.push_back(tmp);
    }
    return vocab;
}
