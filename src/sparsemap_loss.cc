#include <Eigen/Eigen>
#include <Eigen/Dense>
#include <ad3/FactorGraph.h>
#include <ad3/GenericFactor.h>
#include <dynet/nodes-impl-macros.h>

#include "sparsemap_loss.h"

using namespace dynet;
namespace ei = Eigen;
using std::string;
using std::vector;


size_t
SparseMAPLoss::aux_storage_size()
const
{
    return sizeof(float) * (n_unary + n_add);
}


string
SparseMAPLoss::as_string(
    const vector<string>& arg_names)
const
{
    std::ostringstream s;
    s << "sparsemap_loss["
      << get_name()
      << "](";

    for (auto && arg_name : arg_names)
        s << arg_name << ", ";

    s << "cost_augment=" << cost_augment_ << ")";
    return s.str();
}


Dim
SparseMAPLoss::dim_forward(
    const vector<Dim> &xs)
const
{
    run_dimension_check(xs);
    return Dim({1});
}


template<class MyDevice>
void SparseMAPLoss::forward_dev_impl(
    const MyDevice& dev,
    const vector<const Tensor*>& xs,
    Tensor& fx)
const
{
    auto x_u = vec(*xs[0]);
    auto out = vec(fx);

    AD3::FactorGraph factor_graph;
    // factor_graph.SetVerbosity(3);
    vector<AD3::BinaryVariable*> vars;

    for (int k = 0; k < n_unary; ++k)
        vars.push_back(factor_graph.CreateBinaryVariable());

    vector<double> unaries_in(x_u.data(), x_u.data() + n_unary);
    vector<double> add_in;

    if (n_add > 0)
    {
        auto x_v = vec(*xs[1]);
        add_in = vector<double>(x_v.data(), x_v.data() + n_add);
    }

    vector<double> grad_u(n_unary, 0);
    vector<double> grad_v(n_add, 0);

    auto factor = build_factor();
    factor_graph.DeclareFactor(factor.get(), vars, false);
    factor->SetQPMaxIter(100);
    factor->SetClearCache(false);
    factor->SetAdditionalLogPotentials(add_in);

    factor->SolveQP(unaries_in, add_in, &grad_u, &grad_v);

    // compute 0.5 ( ||u_true||^2 - ||u*||^2)
    double val = factor->CountCommonValues(true_cfg, true_cfg);
    for (int k = 0; k < grad_u.size(); ++k)
        val -= grad_u[k] * grad_u[k];
    val *= 0.5;

    factor->UpdateMarginalsFromConfiguration(true_cfg,
                                             -1,
                                             &grad_u,
                                             &grad_v);

    // [eta_u; eta_v] * [u - u_true, v - v_true]
    for (int k = 0; k < n_unary; ++k)
        val += grad_u[k] * unaries_in[k];

    for (int k = 0; k < n_add; ++k)
        val += grad_v[k] * add_in[k];

    auto grad_mem = static_cast<float*>(aux_mem);
    std::copy(grad_u.begin(), grad_u.end(), grad_mem);

    if (n_add > 0)
        std::copy(grad_v.begin(), grad_v.end(), grad_mem + n_unary);

    out(0) = val;

}


template <class MyDevice>
void SparseMAPLoss::backward_dev_impl(
    const MyDevice& dev,
    const vector<const Tensor*>& xs,
    const Tensor& fx,
    const Tensor& dEdf,
    unsigned i,
    Tensor& dEdxi)
const
{
    float* grad = static_cast<float*>(aux_mem);
    auto out = vec(dEdxi);
    if (i == 0)
    {
        for (int k = 0; k < n_unary; ++k)
            out(k) += grad[k];
    }
    else if (i == 1)
    {
        grad += n_unary;
        for (int k = 0; k < n_add; ++k)
            out(k) += grad[k];
    }
}

DYNET_NODE_INST_DEV_IMPL(SparseMAPLoss);
