# pragma once

namespace dy = dynet;

using dy::Expression;
using dy::Parameter;
using dy::LookupParameter;

using std::vector;
using std::unique_ptr;

#include <dynet/dynet.h>
#include <dynet/rnn.h>
#include <dynet/lstm.h>
#include <dynet/tensor.h>
#include <dynet/tensor-eigen.h>

#include "dyutils.h"
#include "char_encoder.h"
#include "evaluate.h"

#include "seq_loss.h"
#include "semimarkov_loss.h"


struct BaseTokenizer : public BaseModel
{

    std::unique_ptr<BiLSTMCharEncoder> char_encoder;
    unsigned hidden_dim_;
    unsigned enc_dim_;

    explicit
    BaseTokenizer(
        dy::ParameterCollection& params,
        unsigned vocab_size,
        unsigned hidden_dim,
        unsigned enc_dim)
        : hidden_dim_(hidden_dim)
        , enc_dim_(enc_dim)
    {
        p = params.add_subcollection("tokenizer");
        char_encoder.reset(new BiLSTMCharEncoder(
            p,
            vocab_size,
            hidden_dim,
            enc_dim));
    }

    /* Abstract functions: to be implemented by each variant */
    virtual
    void
    new_graph(
        dy::ComputationGraph& cg)
    { }

    virtual
    vector<vector<Expression> >
    batch_potentials(dy::ComputationGraph& cg,
                     const vector<Sentence>& batch)
    = 0;

    virtual
    void
    instance_loss(
        const vector<Expression>& potentials, const Sentence& sent,
        vector<Expression>& losses)
    = 0;

    virtual
    vector<segment_t>
    instance_decode(
        const dy::Tensor& potentials,
        size_t length)
    = 0;

    /* Generic functions */
    virtual
    Expression
    batch_loss(
        dy::ComputationGraph& cg,
        const vector<Sentence>& batch)
    {
        new_graph(cg);
        auto potentials = batch_potentials(cg, batch);
        vector<Expression> losses;

        for (size_t k = 0; k < batch.size(); ++k)
            instance_loss(potentials[k], batch[k], losses);
        return dy::sum(losses);
    }

    virtual
    void count_correct(dy::ComputationGraph& cg,
                       const vector<Sentence>& batch,
                       SegmentF1& accum)
    {
        new_graph(cg);
        auto potentials = batch_potentials(cg, batch);

        vector<Expression> Y;
        for (size_t k = 0; k < batch.size(); ++k)
            Y.push_back(dy::concatenate_cols(potentials[k]));

        incremental_forward_all(cg, Y);

        for (size_t k = 0; k < batch.size(); ++k)
        {
            auto segs = instance_decode(Y[k].value(), batch[k].size());
            accum.update(batch[k], segs);

            /*
            std::cout << "predicted " << std::endl;
            for (auto && seg: segs)
                std::cout << std::get<0>(seg) << " "
                          << std::get<1>(seg) << " "
                          << std::get<2>(seg) << std::endl;
            std::cout << "true " << std::endl;
            for (auto && seg: batch[k].segments())
                std::cout << std::get<0>(seg) << " "
                          << std::get<1>(seg) << " "
                          << std::get<2>(seg) << std::endl;
            std::cout << std::endl;
            */
        }
    }
};


struct CharLevelTokenizer : public BaseTokenizer
{
    dy::Parameter p_out_W;
    dy::Parameter p_out_b;

    unsigned n_tags_;
    unsigned n_enc_tags_;
    std::unique_ptr<TagScheme> tag_scheme;

    explicit
    CharLevelTokenizer(
        dy::ParameterCollection& params,
        unsigned n_tags,
        unsigned vocab_size,
        unsigned hidden_dim,
        std::string scheme)
        : BaseTokenizer(params, vocab_size, hidden_dim, hidden_dim)
        , n_tags_(n_tags)
    {
        if (scheme == "bio")
        {
            tag_scheme.reset(new BIO(n_tags_));
        }
        else if (scheme == "bilou")
        {
            tag_scheme.reset(new BILOU(n_tags_));
        }
        else
        {
            std::cout << "Invalid tag scheme." << std::endl;
            std::abort();
        }
        n_enc_tags_ = tag_scheme->n_enc_tags();
        p_out_W = p.add_parameters({n_enc_tags_, hidden_dim_});
        p_out_b = p.add_parameters({n_enc_tags_});
    }

    virtual
    vector<vector<Expression> >
    batch_potentials(dy::ComputationGraph& cg,
                     const vector<Sentence>& batch)
    override
    {
        auto out_W = dy::parameter(cg, p_out_W);
        auto out_b = dy::parameter(cg, p_out_b);

        auto enc = char_encoder->encode_batch(cg, batch);

        vector<vector<Expression> > potentials;
        for (int k = 0; k < batch.size(); ++k)
        {
            vector<Expression> sent_potentials;

            for (size_t i = 0;
                 i < batch[k].char_ixs.size();
                 ++i)
            {
                auto enc_i = enc[k][i];
                auto p_i = dy::affine_transform({out_b, out_W, enc_i});
                sent_potentials.push_back(p_i);
            }
            potentials.push_back(sent_potentials);
        }
        return potentials;
    }
};


struct SimpleTokenizer : public CharLevelTokenizer
{

    using CharLevelTokenizer::CharLevelTokenizer;

    virtual
    void
    instance_loss(
        const vector<Expression>& potentials,
        const Sentence& sent,
        vector<Expression>& losses)
    {
        auto n = potentials.size();
        auto y_seg = sent.segments();
        auto y_tag = tag_scheme->segmentation_to_tags(y_seg, n);
        for (size_t i = 0; i < n; ++i)
        {
            auto p_i = potentials[i];
            auto loss = dy::pickneglogsoftmax(p_i, y_tag[i]);
            losses.push_back(loss);
        }
    }

    virtual
    vector<segment_t>
    instance_decode(
        const dy::Tensor& potentials,
        size_t length)
    override
    {
        auto Yk = dy::mat(potentials);
        vector<unsigned> yt(Yk.cols());
        for (size_t j = 0; j < Yk.cols(); ++j)
            Yk.col(j).maxCoeff(&yt[j]);
        return tag_scheme->tags_to_segmentation(yt);
    }

};


struct SequenceTokenizer : public CharLevelTokenizer
{

    dy::Parameter p_init, p_final, p_trans;
    dy::Expression init_, final_, trans_;

    explicit
    SequenceTokenizer(
        dy::ParameterCollection& params,
        unsigned n_tags,
        unsigned vocab_size,
        unsigned hidden_dim,
        std::string scheme)
        : CharLevelTokenizer(params, n_tags, vocab_size, hidden_dim, scheme)
    {
        p_init = p.add_parameters({n_enc_tags_});
        p_final = p.add_parameters({n_enc_tags_});
        p_trans = p.add_parameters({n_enc_tags_ * n_enc_tags_});
    }

    virtual
    void
    new_graph(
        dy::ComputationGraph& cg)
    override
    {
        init_ = dy::parameter(cg, p_init);
        final_ = dy::parameter(cg, p_final);
        trans_ = dy::parameter(cg, p_trans);
    }

    virtual
    void
    instance_loss(
        const vector<Expression>& potentials,
        const Sentence& sent,
        vector<Expression>& losses)
    {
        auto n = potentials.size();
        auto y_seg = sent.segments();
        auto y_tag = tag_scheme->segmentation_to_tags(y_seg, n);

        vector<Expression> additionals;
        additionals.push_back(init_);
        for (int k = 1; k < n; ++k)
            additionals.push_back(trans_);
        additionals.push_back(final_);

        auto x_u = dy::concatenate_cols(potentials);
        auto x_v = dy::concatenate(additionals);

        auto loss = sequence_sparsemap_loss(x_u, x_v, y_tag);

        // std::cout << loss.value() << std::endl;
        losses.push_back(loss);
    }

    virtual
    vector<segment_t>
    instance_decode(
        const dy::Tensor& potentials,
        size_t length)
    override
    {
        auto x_u = dy::mat(potentials);
        auto vec_init = vec(init_.value());
        auto vec_final = vec(final_.value());
        auto vec_trans = vec(trans_.value());
        vector<unsigned> yt = sequence_decode(x_u,
                                              vec_init,
                                              vec_final,
                                              vec_trans);
        return tag_scheme->tags_to_segmentation(yt);
    }
};


struct SegmentTokenizer : public BaseTokenizer
{

    unsigned n_tags_;
    unsigned max_segment_;
    unsigned segment_vec_dim_;
    unsigned context_;

    dy::Parameter p_out_W;
    dy::Parameter p_out_b;
    dy::Parameter p_hid_W;
    dy::Parameter p_hid_b;
    dy::Parameter p_pad;
    dy::LookupParameter p_len;

    dy::Parameter p_trans;
    dy::Expression trans_;

    explicit
    SegmentTokenizer(
        dy::ParameterCollection& params,
        unsigned n_tags,
        unsigned vocab_size,
        unsigned hidden_dim,
        unsigned max_segment=20,
        unsigned length_embed_dim=10,
        unsigned context=2)
        : BaseTokenizer(params, vocab_size, hidden_dim, hidden_dim / (1 + 2 * context))
        , n_tags_(n_tags)
        , max_segment_(max_segment)
        , segment_vec_dim_(length_embed_dim + enc_dim_ * (1 + 2 * context))
        , context_(context)
    {
        p_trans = p.add_parameters({n_tags * (n_tags + 2)});

        p_len = p.add_lookup_parameters(max_segment, {length_embed_dim});
        p_pad = p.add_parameters({enc_dim_});
        p_hid_W = p.add_parameters({hidden_dim_, segment_vec_dim_});
        p_hid_b = p.add_parameters({hidden_dim_});
        p_out_W = p.add_parameters({n_tags_, hidden_dim_});
        p_out_b = p.add_parameters({n_tags_});
    }

    virtual
    vector<vector<Expression> >
    batch_potentials(dy::ComputationGraph& cg,
                     const vector<Sentence>& batch)
    override
    {
        using dy::parameter;
        using dy::concatenate;
        using dy::affine_transform;
        using dy::tanh;

        auto out_W = parameter(cg, p_out_W);
        auto out_b = parameter(cg, p_out_b);
        auto hid_W = parameter(cg, p_hid_W);
        auto hid_b = parameter(cg, p_hid_b);
        auto pad = parameter(cg, p_pad);

        auto enc = char_encoder->encode_batch(cg, batch);

        vector<vector<Expression> > potentials;
        for (int k = 0; k < batch.size(); ++k)
        {
            // make a vector for each possible segment (a, b)
            vector<Expression> s_potentials;
            unsigned sz = enc[k].size();

            Expression accum, sv, len_vec;
            for (unsigned start = 0; start < sz; ++start)
            {
                accum = enc[k][start];

                unsigned seq_length = 1;
                len_vec = dy::lookup(cg, p_len, seq_length - 1);


                vector<Expression> sv_elems = { len_vec, accum };

                sv_elems.push_back(accum);               // first
                for (unsigned i = 1; i < context_; ++i)   // first + i
                    sv_elems.push_back(pad);

                sv_elems.push_back(accum);               // last
                for (unsigned i = 1; i < context_; ++i)   // last - i
                    sv_elems.push_back(pad);

                sv = concatenate(sv_elems);
                sv = tanh(affine_transform({hid_b, hid_W, sv}));
                s_potentials.push_back(affine_transform({out_b, out_W, sv}));

                for (unsigned cease = start + 1; cease < sz; ++cease)
                {
                    unsigned seg_length = cease - start + 1;
                    if ((max_segment_ > 0) && (seg_length > max_segment_))
                        break;

                    accum = accum + enc[k][cease];
                    len_vec = dy::lookup(cg, p_len, seg_length - 1);

                    vector<Expression> sv_elems = { len_vec, accum };

                    for (unsigned i = 0; i < context_; ++i)   // first + i
                    {
                        if (start + i <= cease)
                            sv_elems.push_back(enc[k][start + i]);
                        else
                            sv_elems.push_back(pad);
                    }
                    for (unsigned i = 0; i < context_; ++i)   // last - i
                    {
                        if (cease - i >= start)
                            sv_elems.push_back(enc[k][cease - i]);
                        else
                            sv_elems.push_back(pad);
                    }

                    sv = concatenate(sv_elems) * float(seg_length);  // unsure about this
                    sv = tanh(affine_transform({hid_b, hid_W, sv}));

                    s_potentials.push_back(affine_transform({out_b, out_W, sv}));
                }
            }
            potentials.push_back(s_potentials);
        }
        return potentials;
    }

    virtual
    void
    new_graph(
        dy::ComputationGraph& cg)
    override
    {
        trans_ = dy::parameter(cg, p_trans);
    }


    virtual
    void
    instance_loss(
        const vector<Expression>& potentials,
        const Sentence& sent,
        vector<Expression>& losses)
    {
        auto y_seg = sent.segments();
        auto x_u = dy::concatenate_cols(potentials);
        auto loss = semimarkov_sparsemap_loss(
            x_u,
            trans_,
            y_seg,
            sent.size(),
            max_segment_);
        losses.push_back(loss);
    }

    virtual
    vector<segment_t>
    instance_decode(
        const dy::Tensor& potentials,
        size_t length)
    override
    {
        auto x_u = dy::mat(potentials);
        auto x_v = dy::vec(trans_.value());
        vector<segment_t> yt = semimarkov_decode(
            x_u,
            x_v,
            length,
            max_segment_);
        return yt;
    }
};
