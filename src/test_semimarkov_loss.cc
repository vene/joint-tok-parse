#include <vector>
#include <dynet/grad-check.h>
#include "semimarkov_loss.h"

using std::vector;
using std::cout;
using std::endl;



int main(int argc, char** argv)
{
    auto dyparams = dy::extract_dynet_params(argc, argv);
    dy::initialize(dyparams);

    const int n_words=5, n_tags=3, max_segment=4;
    int n_segments=0;

    for (int start = 0; start < n_words; ++start)
    {
        for (int cease = start; cease < n_words; ++cease) {

            if ((cease - start + 1) > max_segment)
                continue;

            ++n_segments;
        }
    }

    vector<segment_t> y_true(3);
    y_true[0] = std::make_tuple(0, 0, 2);
    y_true[1] = std::make_tuple(1, 3, 3);
    y_true[2] = std::make_tuple(0, 4, 4);

    dy::ParameterCollection p;
    auto param_x_u = p.add_parameters({n_tags, n_segments});

    {
        dy::ComputationGraph cg;
        auto x_u = dy::parameter(cg, param_x_u);

        auto y = semimarkov_sparsemap_loss(x_u, y_true, n_words, max_segment);

        cg.forward(y);
        cout << y.value() << endl;
        cg.backward(y);
        check_grad(p, y, 1);

        // cout << x_u.gradient() << endl << endl;
        // cout << x_v.gradient() << endl << endl;
    }

    // sparsemap::FactorSegmentationFull ff;
    // ff.Initialize(n_words, n_tags, max_segment);
    //
    int n_add = n_segments * n_tags * 2 + (n_segments-1) * n_tags * n_tags;
    auto param_x_v = p.add_parameters({n_add});

    {
        dy::ComputationGraph cg;
        auto x_u = dy::parameter(cg, param_x_u);
        auto x_v = dy::parameter(cg, param_x_v);

        auto y = semimarkov_trans_loss(x_u, x_v,  y_true, n_words, max_segment);

        cg.forward(y);
        cout << y.value() << endl;
        cg.backward(y);
        check_grad(p, y, 1);
    }
}
