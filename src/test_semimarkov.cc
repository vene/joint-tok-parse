#include <dynet/init.h>
#include <dynet/tensor.h>
#include "semimarkov.h"
#include <ad3/FactorGraph.h>
#include "../../sparsemap/dynet-sparsemap/semimarkov.h"

using std::vector;
using std::cout;
using std::endl;

namespace dy = dynet;


int main(int argc, char** argv)
{
    auto dyparams = dy::extract_dynet_params(argc, argv);
    dy::initialize(dyparams);

    // build unary log potentials
    vector<double> eta_u;
    const int n_words=5, n_tags=3, max_segment=4;
    int n_segments = 0;

    for (int start = 0; start < n_words; ++start)
    {
        for (int cease = start; cease < n_words; ++cease) {

            if ((cease - start + 1) > max_segment)
                continue;
            ++n_segments;
            for (int t = 0; t < n_tags; ++t)
                eta_u.push_back(dy::rand_normal());

        }
    }

    for (auto && x : eta_u) cout << x << " ";
    cout << endl;


    // build zero additionals
    int n_initial = max_segment > n_words ? n_words : max_segment;
    int n_add = n_initial * n_tags * 2 +
                (n_segments-n_initial) * n_tags * n_tags;

    vector<double> eta_v(n_add, 0);
    for (auto && x : eta_v) cout << x << " ";
    cout << endl;

    // old factor
    {
        sparsemap::FactorSegmentation f;
        f.Initialize(n_words, n_tags, max_segment);
        auto cfg = f.CreateConfiguration();
        double val;
        f.Maximize(eta_u, eta_v, cfg, &val);

        cout << "Value with old factor: " << val << "\t";

        for (auto && s : *static_cast<vector<sparsemap::Segment>*>(cfg))
            cout << "(" << s.start << ", " << s.cease << ")["
                 << s.state << "], ";
        cout << endl;

    }

    // new factor
    {
        FactorSegmentation f;
        f.Initialize(n_words, n_tags, max_segment);
        auto cfg = f.CreateConfiguration();
        double val;
        f.Maximize(eta_u, eta_v, cfg, &val);

        cout << "Value with new factor: " << val << "\t";

        for (auto && s : *static_cast<vector<Segment>*>(cfg))
            cout << "(" << s.start << ", " << s.cease << ")["
                 << s.state << "], ";
        cout << endl;
    }

    // new factor with additionals
    {
        FactorSegmentationFull f;
        f.Initialize(n_words, n_tags, max_segment);
        auto cfg = f.CreateConfiguration();
        double val;
        f.Maximize(eta_u, eta_v, cfg, &val);

        cout << "w/. full factor, add=0 " << val << "\t";
        for (auto && s : *static_cast<vector<Segment>*>(cfg))
            cout << "(" << s.start << ", " << s.cease << ")["
                 << s.state << "], ";
        cout << endl;
    }
    cout << endl << endl;

    // ***

    // Generate some init, final, trans scores
    vector<double> init, fin, trans;

    for (int t = 0; t < n_tags; ++t)
    {
        init.push_back(dy::rand_normal());
        fin.push_back(dy::rand_normal());
        for (int tp = 0; tp < n_tags; ++tp)
            trans.push_back(dy::rand_normal());
    }

    // populate eta_f for the fully parametrized factor
    vector<double> eta_f_full;

    // initial
    {
        int start = 0;
        for (int cease = start; cease < n_words; ++cease)
        {
            if ((cease - start + 1) > max_segment) continue;

            for (int state = 0; state < n_tags; ++state)
                eta_f_full.push_back(init[state]);
        }
    }

    // general
    for (int start = 1; start < n_words; ++start)
    {
        for (int cease = start; cease < n_words; ++cease)
        {
            if ((cease - start + 1) > max_segment) continue;

            for (int tp = 0; tp < n_tags; ++tp)
                for (int t = 0; t < n_tags; ++t)
                    eta_f_full.push_back(trans[tp * n_tags + t]);
        }
    }

    // final
    int cease = n_words - 1;
    int start_from = max(0, cease - max_segment + 1);
    for (int start = start_from; start < n_words; ++start)
        for (int state = 0; state < n_tags; ++state)
            eta_f_full.push_back(fin[state]);

    for (auto && x : eta_f_full) cout << x << " ";
    cout << endl;
    assert(eta_f_full.size() ==  n_add);

    // decode with full factor
    {
        FactorSegmentationFull f;
        f.Initialize(n_words, n_tags, max_segment);
        auto cfg = f.CreateConfiguration();
        double val;
        f.Maximize(eta_u, eta_f_full, cfg, &val);

        cout << "Full factor value: " << val << "\tcfg:";
        for (auto && s : *static_cast<vector<Segment>*>(cfg))
            cout << "(" << s.start << ", " << s.cease << ")["
                 << s.state << "], ";
        cout << endl;

        vector<double> out_u, out_f;
        AD3::FactorGraph factor_graph;
        // factor_graph.SetVerbosity(3);
        vector<AD3::BinaryVariable*> vars;
        for (int k = 0; k < eta_u.size(); ++k)
            vars.push_back(factor_graph.CreateBinaryVariable());

        factor_graph.DeclareFactor(&f, vars, false);
        f.SetQPMaxIter(100);
        f.SetClearCache(false);
        f.SetAdditionalLogPotentials(eta_f_full);

        f.SolveQP(eta_u, eta_f_full, &out_u, &out_f);

        for (auto && x : out_u) cout << x << " ";
        cout << endl << endl;
        for (auto && x : out_f) cout << x << " ";
        cout << endl;

    }

    // decode with transition factor
    {
        FactorSegmentationTrans f;
        f.Initialize(n_words, n_tags, max_segment);
        auto cfg = f.CreateConfiguration();
        double val;

        vector<double> eta_f_trans = init;
        for (auto && x : trans)
            eta_f_trans.push_back(x);
        for (auto && x : fin)
            eta_f_trans.push_back(x);
        cout << "we provide " << eta_f_trans.size() << endl;

        f.Maximize(eta_u, eta_f_trans, cfg, &val);

        cout << "Trans factor value: " << val << "\tcfg:";
        for (auto && s : *static_cast<vector<Segment>*>(cfg))
            cout << "(" << s.start << ", " << s.cease << ")["
                 << s.state << "], ";
        cout << endl;

        vector<double> out_u, out_f;
        AD3::FactorGraph factor_graph;
        // factor_graph.SetVerbosity(3);
        vector<AD3::BinaryVariable*> vars;
        for (int k = 0; k < eta_u.size(); ++k)
            vars.push_back(factor_graph.CreateBinaryVariable());

        factor_graph.DeclareFactor(&f, vars, false);
        f.SetQPMaxIter(100);
        f.SetClearCache(false);
        f.SetAdditionalLogPotentials(eta_f_trans);

        f.SolveQP(eta_u, eta_f_trans, &out_u, &out_f);

        for (auto && x : out_u) cout << x << " ";
        cout << endl << endl;
        for (auto && x : out_f) cout << x << " ";
        cout << endl;

    }
}
