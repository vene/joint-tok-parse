#pragma once

#include <tuple>
#include <vector>

typedef std::tuple<unsigned, size_t, size_t> segment_t;

struct TagScheme
{
    size_t n_tags_;

    explicit
    TagScheme(
        size_t n_tags)
        : n_tags_(n_tags)
    {};

    virtual
    int
    n_enc_tags()
    = 0;

    virtual
    std::vector<segment_t>
    tags_to_segmentation(
        std::vector<unsigned> y)
    = 0;

    virtual
    std::vector<unsigned>
    segmentation_to_tags(
        std::vector<segment_t>& seg,
        size_t len)
    = 0;
};


struct BIO : public TagScheme
{
    // if n_tags=n we have:
    // O, B-1, ..., B-n, I-1, ..., I-n

    using TagScheme::TagScheme;

    virtual
    int
    n_enc_tags()
    override
    {
        return 1 + 2 * n_tags_;
    }

    virtual
    std::vector<segment_t>
    tags_to_segmentation(
        std::vector<unsigned> y)
    override
    {
        int curr_tag = -1;
        unsigned start = 0;

        std::vector<segment_t> segments;

        for (unsigned i = 0; i < y.size(); ++i) {
            if (curr_tag == -1) {
                if (y[i] == 0) {
                    // we are in O and remain in O
                    continue;
                } else {
                    // either B-k or I-k will start a segment of state k
                    curr_tag = (y[i] - 1) % n_tags_;
                    start = i;
                }
            } else {
                // we are inside a segment. 2 situations
                if ((y[i] > n_tags_)
                    && ((y[i] - 1) % n_tags_) == curr_tag) {

                    // tag is I-k and we are in state k: do nothing
                    continue;
                } else {
                    // in any other situation, some segment just ended
                    long unsigned end = i - 1;
                    segments.push_back(std::tie(curr_tag, start, end));
                    start = i;
                    if (y[i] == 0) {
                        // transition to O state
                        curr_tag = -1;
                    } else {
                        // transition to whatever new state
                        curr_tag = (y[i] - 1) % n_tags_;
                    }
                }
            }
        }

        // leftover final segment
        if (curr_tag != -1)
        {
            long unsigned last = y.size() - 1;
            segments.push_back(std::tie(curr_tag, start, last));
        }

        return segments;
    }

    virtual
    std::vector<unsigned>
    segmentation_to_tags(
        std::vector<segment_t>& seg,
        size_t len)
    override
    {
        std::vector<unsigned> tags(len, 0);
        for (auto && s : seg) {
            unsigned tag;
            size_t start, cease;
            std::tie(tag, start, cease) = s;

            tags[start] = 1 + tag;  // B-tag
            for (int i = start + 1; i <= cease; ++i)
                tags[i] = 1 + n_tags_ + tag;
        }
        return tags;
    }
};


struct BILOU : public TagScheme
{
    // if n_tags=n we have:
    // O, B-1, ..., B-n, I-1, ..., I-n

    using TagScheme::TagScheme;

    virtual
    int
    n_enc_tags()
    override
    {
        return 1 + 4 * n_tags_;
    }

    virtual
    std::vector<segment_t>
    tags_to_segmentation(
        std::vector<unsigned> y)
    override
    {
        int curr_tag = -1;
        unsigned start = 0;

        std::vector<segment_t> segments;

        for (unsigned i = 0; i < y.size(); ++i) {
            if (curr_tag == -1)
            {
                if (y[i] == 0)
                {
                    // we are in O and remain in O
                    continue;
                }
                else if (y[i] > 2 * n_tags_)
                {
                    // L-k and U-k immediately create a unit segment
                    auto tag = (y[i] - 1) % n_tags_;
                    segments.push_back(std::tie(tag, i, i));
                    // and we are back in an outside-of-tag state
                }
                else // B- or I-
                {
                    // either B-k, I-k start a segment of state k
                    curr_tag = (y[i] - 1) % n_tags_;
                    start = i;
                }
            }
            else
            {

                if (y[i] == 1 + n_tags_ + curr_tag) // I-k for same k
                {
                    continue;
                }
                else if (y[i] == 1 + 2 * n_tags_ + curr_tag) // L-k for same k
                {
                    long unsigned end = i;
                    segments.push_back(std::tie(curr_tag, start, end));
                    curr_tag = -1;
                }
                else  // segment over
                {
                    long unsigned end = i - 1;
                    segments.push_back(std::tie(curr_tag, start, end));
                    start = i;
                    curr_tag = -1;

                    // restart loop as if we came from O
                    if (y[i] > 0)
                        i -= 1;
                }


            }
        }

        // leftover final segment
        if (curr_tag != -1)
        {
            long unsigned last = y.size() - 1;
            segments.push_back(std::tie(curr_tag, start, last));
        }

        return segments;
    }

    virtual
    std::vector<unsigned>
    segmentation_to_tags(
        std::vector<segment_t>& seg,
        size_t len)
    override
    {
        std::vector<unsigned> tags(len, 0);
        for (auto && s : seg) {
            unsigned tag;
            size_t start, cease;
            std::tie(tag, start, cease) = s;

            if (start == cease)
            {
                tags[start] = 1 + (3 * n_tags_) + tag;  // U-tag
            }
            else
            {
                tags[start] = 1 + tag;                  // B-tag
                for (int i = start + 1; i < cease; ++i)
                    tags[i] = 1 + n_tags_ + tag;        // I-tag
                tags[cease] = 1 + (2 * n_tags_) + tag;  // L-tag
            }
        }
        return tags;
    }
};
