# pragma once

#include <vector>
#include <string>

#include <dynet/dynet.h>
#include <dynet/expr.h>
#include <dynet/io.h>

void incremental_forward_all(dy::ComputationGraph& cg,
                             std::vector<dy::Expression> exprs)
{
    dy::Expression* max_expr = &(exprs[0]);
    for (auto && expr : exprs)
        if (expr.i > max_expr->i)
            max_expr = &expr;
    cg.incremental_forward(*max_expr);
}

struct BaseModel
{
    dy::ParameterCollection p;

    void save(const std::string filename)
    {
        dy::TextFileSaver s(filename);
        s.save(p);
    }

    void load(const std::string filename)
    {
        dy::TextFileLoader l(filename);
        l.populate(p);
    }
};
