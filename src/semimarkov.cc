#include "semimarkov.h"

bool operator==(Segment const& lhs,
                Segment const& rhs)
{
    return (tie(lhs.start, lhs.cease, lhs.state) ==
            tie(rhs.start, rhs.cease, rhs.state));
}

bool operator!=(Segment const& lhs,
                Segment const& rhs) {
    return !(lhs == rhs);
}

