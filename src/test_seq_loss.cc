#include <vector>
#include <dynet/grad-check.h>
#include "seq_loss.h"

using std::vector;
using std::cout;
using std::endl;

int main(int argc, char** argv)
{
    auto dyparams = dy::extract_dynet_params(argc, argv);
    dy::initialize(dyparams);

    const int n_words=5, n_tags=3;

    vector<unsigned> y_true(n_words, 0);

    dy::ParameterCollection p;
    auto param_x_u = p.add_parameters({n_tags, n_words});
    auto param_x_v = p.add_parameters({n_tags * (2 + (n_words - 1) * n_tags)});

    dy::ComputationGraph cg;
    auto x_u = dy::parameter(cg, param_x_u);
    auto x_v = dy::parameter(cg, param_x_v);

    auto y = sequence_sparsemap_loss(x_u, x_v, y_true);

    cg.forward(y);
    cout << y.value() << endl;
    cg.backward(y);
    check_grad(p, y, 1);

    // cout << x_u.gradient() << endl << endl;
    // cout << x_v.gradient() << endl << endl;

}
