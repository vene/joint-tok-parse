/* Generic SparseMAP loss parametrized by AD3 factor */

#pragma once

#include <vector>
#include <string>
#include <memory>

#include <ad3/GenericFactor.h>
#include <dynet/dynet.h>
#include <dynet/expr.h>
#include <dynet/nodes-def-macros.h>
#include <dynet/tensor-eigen.h>

namespace dy = dynet;
using namespace dynet;

struct SparseMAPLoss : public dy::Node
{

    size_t n_unary;
    size_t n_add;
    bool cost_augment_;
    AD3::Configuration true_cfg;

    explicit
    SparseMAPLoss(
        const std::initializer_list<dy::VariableIndex>& a,
        bool cost_augment)
        : dy::Node(a)
        , cost_augment_(cost_augment)
    {
        this->has_cuda_implemented = false;
    }

    virtual
    std::string
    get_name()
    const =0;

    virtual
    void
    run_dimension_check(
        const std::vector<Dim> &xs)
    const =0;

    virtual
    std::unique_ptr<AD3::GenericFactor>
    build_factor()
    const =0;

    DYNET_NODE_DEFINE_DEV_IMPL()

    size_t
    aux_storage_size()
    const override;
};
