#include <cassert>
#include <memory>
#include "tag_scheme.h"

#include <iostream>


void
test_inverse(
    std::unique_ptr<TagScheme>& ptr,
    std::vector<segment_t> seg,
    size_t len)
{
    auto tags = ptr->segmentation_to_tags(seg, len);
    // for (auto && t : tags) std::cout << t << " ";
    // std::cout << std::endl;
    auto seg_inv = ptr->tags_to_segmentation(tags);

    assert(seg.size() == seg_inv.size());
    for (int i = 0; i < seg.size(); ++i)
        assert(seg[i] == seg_inv[i]);

    // for (auto && s : seg_inv)
    // {
        // unsigned tag;
        // size_t start, cease;
        // std::tie(tag, start, cease) = s;
        // std::cout << "(" << tag
                  // << "," << start
                  // << ":" << cease
                  // << "), ";
    // }
    // std::cout << std::endl;
}


int
main(
    int argc,
    char** argv)
{
    std::unique_ptr<TagScheme> ptr(new BIO(4));
    assert(ptr->n_enc_tags() == 9);

    ptr.reset(new BIO(5));
    assert(ptr->n_enc_tags() == 11);

    {
        std::vector<segment_t> seg;
        seg.push_back(std::make_tuple(2, 2, 3));
        seg.push_back(std::make_tuple(4, 4, 6));
        seg.push_back(std::make_tuple(4, 7, 7));
        seg.push_back(std::make_tuple(4, 8, 9));
        test_inverse(ptr, seg, 12);
        test_inverse(ptr, seg, 10);
    }

    {
        std::vector<segment_t> seg;
        seg.push_back(std::make_tuple(0, 0, 2));
        seg.push_back(std::make_tuple(0, 3, 4));
        test_inverse(ptr, seg, 5);
    }

    {
        std::vector<segment_t> seg;
        seg.push_back(std::make_tuple(2, 0, 3));
        test_inverse(ptr, seg, 4);
    }

    ptr.reset(new BILOU(5));
    assert(ptr->n_enc_tags() == 21);

    {
        std::vector<segment_t> seg;
        seg.push_back(std::make_tuple(0, 0, 2));
        seg.push_back(std::make_tuple(0, 3, 4));
        test_inverse(ptr, seg, 5);
    }

    {
        std::vector<segment_t> seg;
        seg.push_back(std::make_tuple(2, 2, 3));
        seg.push_back(std::make_tuple(4, 4, 6));
        seg.push_back(std::make_tuple(4, 7, 7));
        seg.push_back(std::make_tuple(4, 8, 9));
        test_inverse(ptr, seg, 12);
        test_inverse(ptr, seg, 10);
    }

    {
        std::vector<segment_t> seg;
        seg.push_back(std::make_tuple(2, 0, 3));
        test_inverse(ptr, seg, 4);
    }

}
