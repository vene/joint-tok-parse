#pragma once

#include <vector>
#include <string>
#include <Eigen/Dense>
#include <dynet/nodes-impl-macros.h>

#include <examples/cpp/dense/FactorSequence.h>

#include "sparsemap_loss.h"

namespace ei = Eigen;


struct SequenceSparseMAPLoss : public SparseMAPLoss
{
    std::vector<int> y_true_;

    size_t length_, n_states_;

    explicit
    SequenceSparseMAPLoss(
        const std::initializer_list<dy::VariableIndex>& a,
        size_t length,
        size_t n_states,
        std::vector<unsigned> y_true,
        bool cost_augment)
        : SparseMAPLoss(a, cost_augment)
        , y_true_(y_true.begin(), y_true.end())
        , length_(length)
        , n_states_(n_states)
    {
        n_unary = length * n_states;
        n_add = n_states * 2                          // initial, final
                + (length - 1) * n_states * n_states; // every transition

        true_cfg = static_cast<AD3::Configuration>(&y_true_);
    }

    virtual
    std::string
    get_name()
    const override
    {
        return "sequence";
    }

    virtual
    void
    run_dimension_check(
        const std::vector<Dim> &xs)
    const override
    {
        DYNET_ARG_CHECK(xs.size() == 2, "sequence loss requires two inputs");
        DYNET_ARG_CHECK(xs[0][0] == n_states_, "unaries have wrong first dim");
        DYNET_ARG_CHECK(xs[0][1] == length_, "unaries have wrong second dim");
        DYNET_ARG_CHECK(xs[1][0] == n_add, "additionals have wrong dimension");
    }

    virtual
    std::unique_ptr<AD3::GenericFactor>
    build_factor()
    const
    {
        std::vector<int> vec_num_states(length_, n_states_);
        std::unique_ptr<AD3::GenericFactor> seq(new AD3::FactorSequence);
        (static_cast<AD3::FactorSequence*>(seq.get()))->Initialize(vec_num_states);
        return seq;
    }

};


dy::Expression
sequence_sparsemap_loss(
    const dy::Expression& x_unary,
    const dy::Expression& x_add,
    std::vector<unsigned> y_true)
{
    auto cg = x_unary.pg;
    auto d = x_unary.dim();

    return dy::Expression(
        cg,
        cg->add_function<SequenceSparseMAPLoss>(
            {x_unary.i, x_add.i},
            d[1],  // length
            d[0],  // n_states
            y_true,
            false));
}


vector<unsigned>
sequence_decode(
    ei::MatrixXf x_unary,
    ei::VectorXf init,
    ei::VectorXf fin,
    ei::VectorXf trans)
{
    size_t n_tags = x_unary.rows();
    size_t length = x_unary.cols();

    vector<double> x_u(x_unary.data(), x_unary.data() + length * n_tags);
    vector<double> x_v(init.data(), init.data() + n_tags);
    for (int k = 1; k < length; ++k)
        x_v.insert(x_v.end(), trans.data(), trans.data() + (n_tags * n_tags));
    x_v.insert(x_v.end(), fin.data(), fin.data() + n_tags);

    std::vector<int> vec_num_states(length, n_tags);
    auto seq =  AD3::FactorSequence();
    seq.Initialize(vec_num_states);
    vector<int> y_pred(length, -1);
    AD3::Configuration cfg = static_cast<AD3::Configuration>(&y_pred);
    double val;
    seq.Maximize(x_u, x_v, cfg, &val);

    return vector<unsigned>(y_pred.begin(), y_pred.end());
}
