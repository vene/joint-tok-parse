# encoding: utf8
"""Convert .conllu into character-level tokenization and parsing data"""

import os
import tarfile
from collections import Counter

UNK = "UNK"
BOS = "BOS"
EOS = "EOS"
SPACE = " "


def parse_conllu_blob(blob):
    blob = blob.decode('utf8').strip()

    # sentences are separated by one empty line:
    sent_blobs = blob.split("\n\n")
    return (parse_conllu_sent(sent_blob)
            for sent_blob in sent_blobs)


def parse_conllu_sent(sent_blob):
    metadata = {}
    toks = []
    for line in sent_blob.splitlines():
        if line.startswith("#"):
            line = line[1:]
            key, val = line.split("=", 1)
            key, val = key.strip(), val.strip()
            metadata[key] = val
        else:
            toks.append(parse_conllu_line(line))

    return metadata, toks


def parse_conllu_line(line):
    keys = ['id', 'form', 'lemma', 'upos', 'xpos', 'feats',
            'head', 'deprel', 'deps', 'misc']

    return {key: val for key, val in zip(keys, line.split("\t"))}


def reconstruct_sent(metadata, toks):
    def _form(tok, is_last):
        form = tok['form']
        if is_last or ('SpaceAfter=No' in tok['misc']):
            return form
        else:
            return form + " "

    forms = [_form(tok, is_last=k + 1 == len(toks))
             for k, tok in enumerate(toks)]
    forms_str = ''.join(forms)

    ref_str = metadata['text']

    # sanity checking performed here. We attempt to reconstruct full string
    if forms_str != ref_str:
        print("mismatch")
        print(forms_str, "__")
        print(ref_str, "__")
        print()

    # and we verify that the lengths are a correct partition
    lengths = [len(form) for form in forms]

    k = 0
    for i, dist in enumerate(lengths):
        if forms[i] != ref_str[k : k + dist]:
            print(forms[i], "__")
            print(ref_str[k : k + dist], "__")

        k += dist

    return lengths


paths = {
    'ja': {
        'train': 'ud-treebanks-conll2017/UD_Japanese/ja-ud-train.conllu',
        'valid': 'ud-treebanks-conll2017/UD_Japanese/ja-ud-dev.conllu',
        'test': [
            'ud-test-v2.0-conll2017/gold/conll17-ud-test-2017-05-09/ja.conllu',
            'ud-test-v2.0-conll2017/gold/conll17-ud-test-2017-05-09/ja_pud.conllu',
        ]
    },

    'zh': {
        'train': 'ud-treebanks-conll2017/UD_Chinese/zh-ud-train.conllu',
        'valid': 'ud-treebanks-conll2017/UD_Chinese/zh-ud-dev.conllu',
        'test': [
            'ud-test-v2.0-conll2017/gold/conll17-ud-test-2017-05-09/zh.conllu',
        ]
    },

    'vi': {
        'train': 'ud-treebanks-conll2017/UD_Vietnamese/vi-ud-train.conllu',
        'valid': 'ud-treebanks-conll2017/UD_Vietnamese/vi-ud-dev.conllu',
        'test': [
            'ud-test-v2.0-conll2017/gold/conll17-ud-test-2017-05-09/vi.conllu',
        ]
    },
}


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('lang', metavar='lang', choices=['ja', 'zh', 'vi'])
    parser.add_argument('--root')
    parser.add_argument('--out', default="./data")
    opts = parser.parse_args()

    def extract(data, sort=True):
        out = []

        for metadata, toks in parse_conllu_blob(data):
            out.append((
                metadata['text'],
                reconstruct_sent(metadata, toks),
                [-1] + [int(tok['head']) for tok in toks],
                [tok['upos'] for tok in toks],
                metadata['sent_id']))

        if sort:
            out.sort(key=lambda x: len(x[0]))

        return out

    # read train and dev
    fn = os.path.join(opts.root, 'ud-treebanks-conll2017.tgz')
    with tarfile.open(fn) as tarf:
        train = tarf.extractfile(paths[opts.lang]['train']).read()
        valid = tarf.extractfile(paths[opts.lang]['valid']).read()

    # read test
    fn = os.path.join(opts.root, 'ud-test-v2.0-conll2017.tgz')
    with tarfile.open(fn) as tarf:
        test = {test_fn: tarf.extractfile(test_fn).read()
                for test_fn in paths[opts.lang]['test']}

    out_tpl = os.path.join(opts.out, f"{opts.lang}-{{}}.txt")

    train_data = extract(train)
    valid_data = extract(valid)

    # double-check spacing issues
    # ensure that our designated UNK character doesn't appear elsewhere
    for sent, _, _, _, _ in train_data:
        for c in sent:
            if c.isspace() and c != SPACE:
                print(c)

            assert c != UNK and c != BOS and c != EOS

    pos_counter = Counter(w
                          for _, _, _, sent, _ in train_data
                          for w in sent)

    train_counter = Counter(c for sent, _, _, _, _ in train_data for c in sent
                            if not c.isspace())

    pos_vocab = [UNK, BOS, EOS] + [c for c, _ in pos_counter.most_common()]
    pos_bacov = {c: k for k, c in enumerate(pos_vocab)}
    vocab = [c for c, count in train_counter.most_common() if count > 1]
    actual_vocab_set = set(vocab)

    vocab = [SPACE, UNK, BOS, EOS] + vocab
    bacov = {c: k for k, c in enumerate(vocab)}

    def _vectorize_char(c):
        if c.isspace():
            return bacov[SPACE]
        elif c not in actual_vocab_set:
            return bacov[UNK]
        else:
            return bacov[c]

    def _vectorize_pos(p):
        return pos_bacov.get(p, pos_bacov[UNK])

    with open(out_tpl.format("vocab"), "w") as f:
        for c in vocab:
            print(c, file=f)

    with open(out_tpl.format("posvocab"), "w") as f:
        for c in pos_vocab:
            print(c, pos_counter.get(c, -1), sep="\t", file=f)

    def write_out(f, data):
        for sent, tok_ix_, tree, upos, sent_id in data:
            char_ix = " ".join(str(_vectorize_char(c))
                               for c in sent)
            tree = " ".join(str(i) for i in tree)
            tok_ix_ = " ".join(str(i) for i in tok_ix_)
            pos = " ".join(str(_vectorize_pos(p)) for p in upos)

            fields = [char_ix, tok_ix_, pos, tree, sent, sent_id]
            for field in fields:
                assert "\t" not in field
            print("\t".join(fields), file=f)

    with open(out_tpl.format("train"), "w") as f:
        write_out(f, train_data)

    with open(out_tpl.format("valid"), "w") as f:
        write_out(f, valid_data)

    for test_fn, test_blob in test.items():
        test_data = extract(test_blob)
        identifier = os.path.splitext(os.path.basename(test_fn))[0]

        with open(out_tpl.format("test-{}".format(identifier)), "w") as f:
            write_out(f, test_data)
